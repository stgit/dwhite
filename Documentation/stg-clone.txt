stg-clone(1)
==========
Yann Dirson <ydirson@altern.org>
v0.12.1, April 2007

NAME
----
stg-clone - stgdesc:clone[]

SYNOPSIS
--------
[verse]
'stg' clone <repository> <dir>

DESCRIPTION
-----------

Clone a GIT <repository> into the local <dir> (using
gitlink:git-clone[1]) and initialise the resulting "master" branch as
a patch stack.

This operation is for example suitable to start working using the
"tracking branch" workflow (see gitlink:stg[1]).  Other means to setup
an StGIT stack are stglink:init[] and the '--create' and '--clone'
commands of stglink:branch[].

The target directory named by <dir> will be created by this command,
and must not exist beforehand.

StGIT
-----
Part of the StGIT suite - see gitlink:stg[1].
