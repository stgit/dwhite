stg-sink(1)
===========
Yann Dirson <ydirson@altern.org>
v0.13, April 2007

NAME
----
stg-sink - stgdesc:sink[]

SYNOPSIS
--------
[verse]
'stg' sink [--to=<target>] [--nopush] [<patches>]

DESCRIPTION
-----------

This is the opposite operation of stglink:float[]: move the specified
patches down the stack.  It is for example useful to group stable
patches near the bottom of the stack, where they are less likely to be
impacted by the push of another patch, and from where they can be more
easily committed or pushed.

If no patch is specified on command-line, the current patch gets sunk.
By default patches are sunk to the bottom of the stack, but the
'--to' option allows to place them under any applied patch.

Sinking internally involves popping all patches (or all patches
including <target patch>), then pushing the patches to sink, and then
(unless '--nopush' is also given) pushing back into place the
formerly-applied patches.


OPTIONS
-------

--to=<TARGET>::
-t <TARGET>::
	Specify a target patch to place the patches below, instead of
	sinking them to the bottom of the stack.

--nopush::
-n::
	Do not push back on the stack the formerly-applied patches.
	Only the patches to sink are pushed.

StGIT
-----
Part of the StGIT suite - see gitlink:stg[1].
